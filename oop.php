<?php

trait Hewan {
    public $nama;
    public $darah = 50;
    public $jumlahKaki;
    public $keahlian;

    //public function __construct($nama,  $darah, $jumlahKaki, $keahlian){
    //    $this->nama         = $nama;
    //    $this->darah        = $darah;
    //    $this->jumlahKaki   = $jumlahKaki;
    //    $this->keahlian     = $keahlian;
    //}
    public function atraksi(){
        return $this->nama ." sedang " .$this->keahlian;
    }

    
}

trait Fight {
    use Hewan;
    public $attackPower;
    public $defencePower;

    //public function __construct($attackPower,  $defencePower){
    //    $this->attackPower   = $attackPower;
    //     $this->defencePower  = $defencePower;
    //}

    public function serang($penyerang, $korban){
        return $penyerang . "sedang menyerang" . $korban ."<br>";
    }

    public function diserang($korban, $penyerang, $darahKorban, $attackPower, $defencePower){
        $darahKorban = $darahKorban - ($attackPower/$defencePower);
        return $korban . "sedang diserang" . $penyerang. "<br>";

    }
}

class Harimau{
    use Hewan, Fight;
    public function getInfoHewan(){
        return 'Nama Hewan = ' . $this->nama . '<br>' . 'Jumlah Darah = ' . $this->darah . '<br>' . 'Jumlah Kaki =' . $this->jumlahKaki . '<br>' . 'Keahlian = ' . $this->keahlian . '<br>' .'AttackPower = ' . $this->attackPower . '<br>' . 'DefencePower = ' .$this->defencePower;
    }
}

class Elang{
    use Hewan, Fight;
    public function getInfoHewan(){
        return 'Nama Hewan = ' . $this->nama . '<br>' . 'Jumlah Darah = ' . $this->darah . '<br>' . 'Jumlah Kaki =' . $this->jumlahKaki . '<br>' . 'Keahlian = ' . $this->keahlian . '<br>' .'AttackPower = ' . $this->attackPower . '<br>' . 'DefencePower = ' .$this->defencePower;
    }
}



//menggunakan metode construct
//$elang = new Fight(2,"terbang tinggi",10,5);

//apabila tidak menggunakan construct
$elang = new Elang();
$elang->nama          = "Elang";
$elang->jumlahKaki    = 2;
$elang->keahlian      = "terbang tinggi";
$elang->attackPower   = 10;
$elang->defencePower  = 5 ;
//var_dump($elang);

//menggunakan metode construct
//$harimau = new Fight(4,"lari cepat", 7, 8);

//apabila tidak menggunakan construct
$harimau = new Harimau();
$harimau->nama        = "Harimau";
$harimau->jumlahKaki  = 4;
$harimau->keahlian    = "lari cepat";
$harimau->attackPower = 7;
$harimau->defencePower= 8 ;
//var_dump($harimau);


echo $harimau->getInfoHewan();
echo "<br><br>";
echo $elang->getInfoHewan();
echo "<br><br>";

echo "<b>Method atraksi()</b><br>";
echo $harimau->atraksi();

echo "<br><br>";
echo "<b>Method serang()</b><br>";
$penyerang = $harimau->nama . ' ';
$korban    = ' ' .$elang->nama ;
echo $harimau->serang($penyerang, $korban);

echo "<br>";
echo "<b>Method diserang()</b><br>";
$penyerang   = ' '.$harimau->nama;
$korban      = $elang->nama. ' ' ;
$darah       =$elang->darah;
$attackPower = $harimau->attackPower;
$defencePower = $elang->defencePower;
$darahKorban = $darah - ($attackPower/$defencePower );
echo $harimau->diserang($korban, $penyerang, $darahKorban, $attackPower, $defencePower);
echo 'Darah Elang = ' .$darahKorban;


?>